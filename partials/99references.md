# Considerações finais

+++
## ReactiveX (Rxjs)
![rxjs](https://avatars1.githubusercontent.com/u/984368?s=400&v=4)

---
# Bibliotecas

- [Ramda](http://ramdajs.com/docs)
- [Lodash](https://lodash.com/docs/)
- [FPO](https://github.com/getify/fpo)
---

# Referências

- [Functional-Light JavaScript - Kyle Simpson](https://github.com/getify/Functional-Light-JS)
- [Prof. Frisby's Mostly Adequate Guide to Functional Programming](https://drboolean.gitbooks.io/mostly-adequate-guide/content/)
- [CIS 194: Introduction to Haskell (Spring 2015) - University of Pennsylvania](http://web.archive.org/web/20150912044431/http://www.seas.upenn.edu:80/~cis194/)
