# Aplicação parcial

+++

```haskell
post :: String -> Object -> (Object -> *) -> Promise
```

```js
ajax.post(
    'http://api.teste.com/users/',
    {
        name: 'Matheus'
    },
    function(result){
        ...
    }
)
```
+++
```js
function createUser(obj, callback){
    ajax.post('http://api.teste.com/users/', obj, callback)
}

// createUser({}, (r) => ...)
```

+++

```js
function partial(fn, ...args){
    return function(...resto){
        return fn(...args, ...resto)
    }
}
```
+++
```haskell
createUser :: Object -> (Object -> *) -> Promise
```
```js
const createUser = partial(ajax.post, 'http://api.teste.com/users/')

// createUser({}, (r) => ...)
```
+++
