# Imutabilidade

+++
@title[const]

A palavra reservada const resolve nossos problemas! 🎉

```js
const a = 2;
a = 3 // TypeError
```

+++
@title[const com listas]

É... mais ou menos 😓

```js
const b = [1,2,3]
b[0] = 3 // Ah, ok, de boa
```
+++
@title[dobraLista mutável]

```js
//[Number] -> undefined
function dobraLista(list){
    for(let i = 0; i < list.length; i++){
        list[i] = list[i] * 2
    }
}
let ls = [1,2,3]
dobraLista(ls)
ls  // 2, 4, 6
```

+++
@title[dobraLista imutável]
```js
//[Number] -> [Number]
function dobraListaPura(list){
    let novaLista = []
    for(let i = 0; i < list.length; i++){
        novaLista[i] = list[i] * 2
    }
    return novaLista
}
let ls1 = [1,2,3]
let ls2 = dobraListaPura(ls1)
console.log(ls1, ls2)
```

Note: Splice/slice
