# Reduce

+++
@title[Reducer]
```haskell
reducer :: a -> b -> a
reduce :: (a -> b -> a) -> a -> [b] -> a
```
```js
filter(reducer)(inicial)(lista)
lista.filter(reducer)(inicial)
```
+++
@title[Reduce com emojis]

```js
[🍔, 🍗, 🍟, 🍿].reduce(eat) === 💩
```
+++
@title[Soma imperativa]
```js
var lista = [1,2,3,4,5]
var soma = 0;
for(let i = 0; i < lista.length; i++){
    soma += lista[i]
}
soma
```
+++
@title[Soma declarativa]
```js
lista.reduce((soma, x) => soma + x, 0)
```
+++
@title[WITCHCRAFT]
![WITCHCRAFT](https://media.giphy.com/media/ZYTAlnmOIE7ja/giphy.gif)
+++
@title[Comparação]

```js
var lista = [1,2,3,4,5]
var soma = 0;
for(let i = 0; i < lista.length; i++){
    soma += lista[i]
}

lista.reduce((soma, x) => soma + x, 0)
```

+++
@title[Usando add]
```js
const add = (x,y) => x + y
lista.reduce(add)
```

+++
@title[Usando mult]
```js
const mult = (x,y) => x * y
lista.reduce(mult)
```
+++
@title[Min e max]
```js
lista.reduce(Math.max)
lista.reduce(Math.min)
```
+++
@title[Min e max com binary]
```js
lista.reduce(binary(Math.max))
lista.reduce(binary(Math.min))
```
```js
Math.max(...lista)
Math.min(...lista)
```

+++
@title[Compose com reduce]
```haskell
reducer :: a -> b -> a
compose2 :: fn -> fn -> fn
```
```js
function compose2(fn1, fn2){
    return function comp(...args){
        return fn1(fn2(...args))
    }
}
```
+++
@title[Compose com reduce]
```js
const compose = (...fns) => fns.reduce(compose2)
```
+++
@title[Exemplo de compose]
```js
const isMult = x => y => y % x === 0
const add = x => y => x + y
const mult = x => y => x * y

const trecoDoido = compose(isMult(3), mult(2), add(1))
```
