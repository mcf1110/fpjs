# Currying

+++

```js
// add :: Number -> Number -> Number

const add = (x, y) => x + y
add(10 ,5) // 15
```

+++

```js
// curriedAdd :: Number -> (Number -> Number)

function curriedAdd(x){
    return function(y){
        return x + y
    }
}

const curriedAdd = x => y => x + y

add(10 ,5) // 15
curriedAdd(10)(5) //15
```
+++

```haskell
       add :: Number -> Number -> Number
curriedAdd :: Number -> (Number -> Number)
```

+++

```haskell
post :: String -> Object -> (Object -> *) -> ()
createUser :: Object -> (Object -> *) -> ()
fireRequest :: (Object -> *) -> ()
```

```js
ajax.post(
    'http://api.teste.com/users/',
    {
        name: 'Matheus'
    },
    function(result){}
)

const createUser = ajax.post('http://api.teste.com/users/')
const fireRequest = createUser(({name: 'Matheus'}))
fireRequest(function(response){
    ...
})
```
+++

```js
ajax.post('http://api.teste.com/users/')
    ({name: 'Matheus'})
    (function(response){
        ...
    })
```

+++

```js
    // Int -> Int -> Bool
const gt = x => y => y > x

// Int -> Bool
const gt10 = gt(10)
gt10(12) // true
gt10(9) // false
```

Note:
Predicado
