# O que é uma função?

- Don't Repeat Yourself |
- Abstração |

+++
@title[Função]

![Função](https://upload.wikimedia.org/wikipedia/commons/d/dc/Allowed_mapping_for_a_function.png)

$$f(x) = x^2$$

Note:

Função/procedimento(procedure)

+++

# Função pura

- Imutabilidade |
- Sem efeitos colaterais |
- Determinístico (mesma entrada, mesma saída) |

Note:
Transparência referencial!


+++
@title[Função não determinística]
```js
var a = 12;

function soma(b){
    return a + b
}

soma(2)
```
Note:
Variável livre

+++
@title[Efeitos colaterais]
```js
var a = 12;

function triplo(b){
    a = 3 * b
}

triplo(2)
```

+++
@title[Efeitos colaterais]

Qual deve ser o resultado dos três consoles.log?

```js
var x = 1;

foo()
console.log(x)
bar()
console.log(x)
baz()
console.log(x)
```
+++

## O que ganhamos com funções puras?

- Pode-se substituir igual por igual |
- Paralelismo |
- Menos dor de cabeça |
- Legibilidade |
