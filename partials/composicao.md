# Composição

$$f{\circ}g(x) = f(g(x))$$

+++
@title=[Imagem composição]
![Composição](http://www.math.toronto.edu/preparing-for-calculus/4_functions/images/composition.png)

+++
@title=[Variável intermediária]
```js
// Number -> Number
const dobro = x => 2 * x
const incrementa = x => x + 1

let intermediario = dobro(7)
incrementa(intermediario) //15
```
+++
@title=[Sem variável intermediária]
```js
// Number -> Number
const dobro = x => 2 * x
const incrementa = x => x + 1
incrementa(dobro(7)) //15
```
+++
@title=[Composição manual]
```js
// Number -> Number
const dobro = x => 2 * x
const incrementa = x => x + 1
const dobroMaisUm = x => incrementa(dobro(x))

dobroMaisUm(7) //15
```
+++
@title=[Compose]
```js
// (a -> b) -> (b -> c) -> (a -> c)
function compose2(fn1, fn2){
    return function comp(...args){
        return fn1(fn2(...args))
    }
}

// Number -> Number
dobroMaisUm = compose2(dobro, incrementa)
```
+++
@title=[Pipe]
```js
//Referencia ao pipe do unix
// cat arquivo | grep test*
function pipe2(fn1, fn2){
    return function piped(...args){
        return fn2(fn1(...args))
    }
}
```
