# Um pouco de sintaxe

+++
## ES6
+++
### Arrow function

```js
function soma(a,b){
    return a + b
}

const mult = (a,b) => a * b
const dobro = a => 2 * a
```
+++
### Spread Operator

```js
let lista = [1,2]

soma(...lista) === soma(lista[0], lista[1])
```
+++
### Destructuring
```js
let [um, dois, tres] = [1,2,3]
let {valor} = {nome: 'Nao ligo pra essa aqui', valor: 12}
```
+++

### Rest Operator
```js
let [primeiro, ...resto] = [1,4,3,2,5,23,4]

function append(lista, ...valores){
    return lista.concat(valores)
}
//append(resto, 1, 2, 4)
```

Note:

Função variadica

+++
## Declarando os tipos

Qual o tipo da função abaixo?

```js
function foo(a){
    return a > 10
}
```
+++
@title[Type Annotation]

```js
// Number -> Bool
function foo(a){
    return a > 10
}
```
Note: Predicado
+++
@title[Type Annotation]

```js
// Number -> String
function bar(x){
    return (x + 1) + " resultados encontrados"
}

// Number -> Number -> Number
function foo(a, b){
    return a + b;
}
```

+++
@title[Type Annotation para listas]

```js
// [a] -> String
function bar(x){
    return x.length + " resultados encontrados"
}
```

+++
@title[Type Annotation com arrow function]
```js
// Number -> Number
const dobro = a => 2 * a
```
