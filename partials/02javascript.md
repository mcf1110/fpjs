# Por que JavaScript?

+++

## Pontos Fortes

- Funções são first-class |
- Closure |
+++
### Funções são first-class

```js
function parseObject(r){
    $('.content').text(r.data)
}
$.get(url).then(parseObject)

//parseObject !== parseObject()
```
+++
### Closure

> Closure é quando uma função se lembra das variáveis que estão ao redor dela, mesmo quando executada em outro lugar

```js
var a = 2;
function closure(r){
    return a * r.value
}
$.get(url).then(closure)
```

Note:
Existe uma linguagem funcional chama Clojure (roda na JVM)

+++

## Pontos Fracos

- Fracamente tipada |
