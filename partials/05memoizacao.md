# Memoização

Eu juro que não escrevi errado.

+++
@title[Motivação]
Se a função tem a mesma saída sempre, pra que rodar ela de novo?
+++
@title[Memoização manual]
```js
function memoizeIsEven(){
    let obj = {}
    return function memoized(x){
        if(obj[x] == undefined){
            console.log('rodando funcao cara')
            obj[x] = x % 2 === 0
        }
        return obj[x]
    }
}
```
+++
@title[Memoização genérica]
```js
function memoize(fn){
    let obj = {}
    return function memoized(x){
        if(obj[x] == undefined){
            obj[x] = fn(x)
        }
        return obj[x]
    }
}
```
