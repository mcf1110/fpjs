# Map
+++
@title[Mapper]
```haskell
mapperFunction :: a -> b
map :: (a -> b) -> [a] -> [b]
```
```js
map(mapperFunction)(lista)
```
+++
@title[Sintaxe em JS]
```js
lista.map(mapperFunction) //meio OO, meio FP
```
+++
@title[Map com emojis]

```js
[🐮, 🐔, 🥔, 🌽].map(cook) === [🍔, 🍗, 🍟, 🍿]
```
+++
@title[Aplicando map]
```js
[1,2,3,4].map(x => 2 * x) // [2,4,6,8]
```
+++
# Filter
+++
@title[Predicado]
```haskell
predicado :: a -> Bool
filter :: (a -> Bool) -> [a] -> [a]
```
```js
filter(predicado)(lista)
lista.filter(predicado)
```
+++
@title[Filter com emojis]
```js
[🍔, 🍗, 🍟, 🍿].filter(isVegetarian) === [🍟, 🍿]
```
+++
@title[Aplicando filter]

```js
[1,2,3,4].filter(x => x % 2 === 0) //[2,4]
[null,0,undefined,'texto', ''].filter(x => x)
```

Note:
Função identidade
