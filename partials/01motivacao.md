# Por que programação funcional?

- Confiança 😉 |
- Legibilidade 📖 |

Note:

"Isso é coisa de matemático!"


+++

## Confiança

Código que não entendemos é código que não podemos confiar. 😈

Note:

Menos bugs!

+++

## Legibilidade

Declarativo/Imperativo

Note:

Não é binário!

Reparem que não falei de performance
