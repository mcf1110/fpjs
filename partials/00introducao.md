# Programação Funcional

+++
@title[Introdução]
Ou melhor,

**Introdução à Programação Funcional em JavaScript**.

*Mas não cabia como h1.* 😁

+++
@title[Qual é o problema?]
> Uma mônada é apenas um monoide na categoria dos endofuntores. Qual é o problema?


Note:

Isso só significa algo para os já iniciados em fp.
Uma breve introdução a padrões funcionais, usando termos só quando necessário.
Absolutismo é um problema.

+++

## Afinal, o que é Programação Funcional?

Note:

Apenas um paradigma
