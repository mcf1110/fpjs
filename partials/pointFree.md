# Point-free Style

+++
```haskell
get :: String -> Promise
then :: (Object -> *) -> Promise
```
```js
ajax.get('').then(function(v){
    console.log(v)
})
```

```js
ajax.get('').then(console.log)
```

+++
```haskell
then :: (Object -> String -> XHR -> *) -> Promise
```
```js
ajax.get('').then(function(response, status, xhr){
    console.log(response, status, xhr)
})

ajax.get('').then(function(response, status, xhr){
    console.log(response)
})
```

+++

## Unary

```js
function unary(fn){
    return function(x){
        return fn(x)
    }
}
//ou
const unary = fn => x => fn(x)
```

+++

```js
ajax.get('').then(unary(console.log))
```
+++

## Binary?

Note:

Aridade: n de parâmetros das funções
Data-last
